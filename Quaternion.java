import java.util.*;

/**
 * The class is destined for the homework assignment no 4 The skeleton is
 * attributed to jpoial. Src https://jpoial@bitbucket.org/i231/kodu4.git the
 * remaining of the code: furunkel027@bitbucket.com === Quaternions. Basic
 * operations. ===
 */
	// isZero meetodis osutatud viga k->r ära parandatud
	// protected asemel pandud muutujad private

	// Vastus, MIKS ma nii tegin: vaatasin sellele lingile otsa:
	// http://stackoverflow.com/questions/215497/in-java-whats-the-difference-between-public-default-protected-and-private
	// ja valisin funktsionaalselt selle variandi, mille piirangud on väiksemad (3y vs 1y)
	// et kusagil mingi müstika otsa mitte joosta. PARANDATUD.

	// Nüüd läbib testid. Thnx.

public class Quaternion {

	private double re;
	private double ii;
	private double jj;
	private double kk;
	// Muutuja, millega nullilähedust võrrelda, as told in PöidlaLoeng ;)
	static final double epsilon = 0.00000001;

	/**
	 * Constructor from four double values.
	 * 
	 * @param a
	 *            real part
	 * @param b
	 *            imaginary part i
	 * @param c
	 *            imaginary part j
	 * @param d
	 *            imaginary part k
	 */
	public Quaternion(double a, double b, double c, double d) {
		// TODO!!! Your constructor here!
		re = a;
		ii = b;
		jj = c;
		kk = d;
	}

	/**
	 * Real part of the quaternion.
	 * 
	 * @return real part
	 */
	public double getRpart() {
		return this.re;
	}

	/**
	 * Imaginary part i of the quaternion.
	 * 
	 * @return imaginary part i
	 */
	public double getIpart() {
		return this.ii;
	}

	/**
	 * Imaginary part j of the quaternion.
	 * 
	 * @return imaginary part j
	 */
	public double getJpart() {
		return this.jj;
	}

	/**
	 * Imaginary part k of the quaternion.
	 * 
	 * @return imaginary part k
	 */
	public double getKpart() {
		return this.kk;

	}

	/**
	 * Conversion of the quaternion to the string.
	 * 
	 * @return a string form of this quaternion: "a+bi+cj+dk" (without any
	 *         brackets)
	 */
	@Override
	public String toString() {
		// Ilma tsüklita jube inetu, aga alguseks kärab kah.
		StringBuilder koosteLiin = new StringBuilder();
		String resultaat;
		String nummer;
		char sign = ' ';

		nummer = Double.toString(re);
		koosteLiin.append(nummer);

		if (ii >= 0) {
			sign = '+';
		} else
			sign = ' ';
		nummer = Double.toString(ii);
		koosteLiin.append(sign);
		koosteLiin.append(nummer);
		koosteLiin.append('i');
		
		if (jj >= 0) {
			sign = '+';
		} else
			sign = ' ';
		nummer = Double.toString(jj);
		koosteLiin.append(sign);
		koosteLiin.append(nummer);
		koosteLiin.append('j');

		if (kk >= 0) {
			sign = '+';
		} else
			sign = ' ';
		nummer = Double.toString(kk);
		koosteLiin.append(sign);
		koosteLiin.append(nummer);
		koosteLiin.append('k');

		resultaat = koosteLiin.toString();
		resultaat = resultaat.replaceAll("\\s+", "");
		return resultaat;
	}

	/**
	 * Conversion from the string to the quaternion. Reverse to
	 * <code>toString</code> method.
	 * 
	 * @throws IllegalArgumentException
	 *             if string s does not represent a quaternion (defined by the
	 *             <code>toString</code> method)
	 * @param s
	 *            string of form produced by the <code>toString</code> method
	 * @return a quaternion represented by string s
	 */
	public static Quaternion valueOf(String s) {
		if (s.length() == 0) throw new IllegalArgumentException("EXCEPTION: string |" + s + "| resembles no quaternion") ;
		boolean minusSignOne = false;
		boolean minusSignTwo = false;
		
		String ss = s.trim();
		
		double r = 0. , i = 0. , j = 0. , k = 0. ;
		String s1, s2, s21, s22, s3, s4;
		
		// Parsing rule:  (((-1.0-1.0) i -2.0) j +3.0) k
		//                       ^     ^       ^       ->
		// neljas jääb lõksu
		String[] smth4 = ss.split("j");
		s4 = smth4[1];
		s4 = s4.substring(0, s4.length() - 1); // k -> away
		k = Double.parseDouble(s4);

		// kolmas jääb lõksu
		String[] smth3 = smth4[0].split("i");
		s3 = smth3[1];
		j = Double.parseDouble(s3) ;
		
		// Siit edasi tõeline raisakullikeemia s2 ja s2 eraldamiseks
		s21 = smth3[0];
		s22 = "";
		
		if (s21.startsWith("-") ) {
			minusSignOne = true;
		}
			s22 = s21.replaceAll("^-", ""); // leading sign -> away

		if (s22.contains("-")) {
			minusSignTwo = true;
			}
			
		String[] smth12 = s22.split("[-|+]"); // sign of i as separator
		s1 = smth12[0];
		s2 = smth12[1];

		if (minusSignOne) {
			s1 = "-" + s1; // recover the sign for s1
		}
		r = Double.parseDouble(s1);
		
		if (minusSignTwo) {
			s2 = "-" + s2; // recover the sign for s2
		}
		i = Double.parseDouble(s2);
			
		Quaternion tadaa = new Quaternion(r, i, j, k);
		return tadaa;
}

	/**
	 * Clone of the quaternion.
	 * 
	 * @return independent clone of <code>this</code>
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return new Quaternion(this.re, this.ii, this.jj, this.kk);
	}

	/**
	 * Test whether the quaternion is zero.
	 * 
	 * @return true, if the real part and all the imaginary parts are (close to)
	 *         zero
	 */
	public boolean isZero() {
		boolean indeed = false;
		// epsilon on konstant, vt ylevalpool
		double r = 0. , i = 0. , j = 0. , k = 0. ;
		
		r = Math.abs(this.re);
		i = Math.abs(this.ii);
		j = Math.abs(this.jj);
		k = Math.abs(this.kk);
		// Tänud vihje eest k->r, ise ei oleks seda paljundamisviga eales märganud:
		if (r <= epsilon) {
			if (i <= epsilon) {
				if (j <= epsilon) {
					if (k <= epsilon) {
						indeed = true;
					}
				}
			}
		}
		return indeed;
	}

	/**
	 * Conjugate of the quaternion. Expressed by the formula
	 * conjugate(a+bi+cj+dk) = a-bi-cj-dk
	 * 
	 * @return conjugate of <code>this</code>
	 */
	public Quaternion conjugate() {
		double minus = -1. ;
		return new Quaternion(this.re, (this.ii * minus), (this.jj * minus), (this.kk * minus));
	}

	/**
	 * Opposite of the quaternion. Expressed by the formula opposite(a+bi+cj+dk)
	 * = -a-bi-cj-dk
	 * 
	 * @return quaternion <code>-this</code>
	 */
	public Quaternion opposite() {
		double minus = -1. ;
		return new Quaternion((this.re * minus), (this.ii * minus), (this.jj * minus), (this.kk * minus));
	}

	/**
	 * Sum of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) +
	 * (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
	 * 
	 * @param q
	 *            addend
	 * @return quaternion <code>this+q</code>
	 */
	public Quaternion plus(Quaternion q) {
		return new Quaternion(this.re + q.re, this.ii + q.ii, 
				this.jj + q.jj, this.kk + q.kk);
	}

	/**
	 * Product of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) *
	 * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
	 * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
	 * 
	 * @param q
	 *            factor
	 * @return quaternion <code>this*q</code>
	 */
	public Quaternion times(Quaternion q) {
		double r = 0. , i = 0. , j = 0. , k = 0. ;
		r = this.re * q.re - this.ii * q.ii - this.jj * q.jj - this.kk * q.kk;
		i = this.re * q.ii + this.ii * q.re + this.jj * q.kk - this.kk * q.jj;
		j = this.re * q.jj - this.ii * q.kk + this.jj * q.re + this.kk * q.ii;
		k = this.re * q.kk + this.ii * q.jj - this.jj * q.ii + this.kk * q.re;
		return new Quaternion( r, i, j, k);

	}

	/**
	 * Multiplication by a coefficient.
	 * 
	 * @param r
	 *            coefficient
	 * @return quaternion <code>this*r</code>
	 */
	public Quaternion times(double r) {
		return new Quaternion((this.re * r), (this.ii * r), (this.jj * r), (this.kk * r));
	}

	/**
	 * Inverse of the quaternion. Expressed by the formula 1/(a+bi+cj+dk) =
	 * a/(a*a+b*b+c*c+d*d) + ((-b)/(a*a+b*b+c*c+d*d))i +
	 * ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
	 * 
	 * @return quaternion <code>1/this</code>
	 */
	public Quaternion inverse() {
		if ( this.isZero() ) {
			throw new ArithmeticException("EXCEPTION: 1 being divided by ZERO is not kosher");
		}
		double r = 0. , i = 0. , j = 0. , k = 0. ;
		double minus = -1. ; 
		
		r = this.re / (this.re * this.re + this.ii * this.ii
				+ this.jj * this.jj + this.kk * this.kk);
		i = (this.ii * minus) / (this.re * this.re
				+ this.ii * this.ii + this.jj * this.jj + this.kk * this.kk);
		j = (this.jj * minus) / (this.re * this.re 
				+ this.ii * this.ii + this.jj * this.jj + this.kk * this.kk);
		k = (this.kk * minus) / (this.re * this.re
				+ this.ii * this.ii + this.jj * this.jj + this.kk * this.kk);
		return new Quaternion(r, i, j, k);
	}

	/**
	 * Difference of quaternions. Expressed as addition to the opposite.
	 * 
	 * @param q
	 *            subtrahend
	 * @return quaternion <code>this-q</code>
	 */
	public Quaternion minus(Quaternion q) {
		double r = 0. , i = 0. , j = 0. , k = 0. ;
		r = this.re - q.re;
		i = this.ii - q.ii;
		j = this.jj - q.jj;
		k = this.kk - q.kk;
		return new Quaternion(r, i, j, k);
	}

	/**
	 * Right quotient of quaternions. Expressed as multiplication to the
	 * inverse.
	 * 
	 * @param q
	 *            (right) divisor
	 * @return quaternion <code>this*inverse(q)</code>
	 */
	public Quaternion divideByRight(Quaternion q) {
		Quaternion inverted = q.inverse();
		return this.times(inverted);
	}

	/**
	 * Left quotient of quaternions.
	 * 
	 * @param q
	 *            (left) divisor
	 * @return quaternion <code>inverse(q)*this</code>
	 */
	public Quaternion divideByLeft(Quaternion q) {
		Quaternion inverted = q.inverse();
		return inverted.times(this);
	}

	/**
	 * Equality test of quaternions. Difference of equal numbers is (close to)
	 * zero.
	 * 
	 * @param qo
	 *            second quaternion
	 * @return logical value of the expression <code>this.equals(qo)</code>
	 */
	@Override
	public boolean equals(Object qo) {
		// Mis seal pattu salata, see OOP lause on Ylarilt võetud:
		// http://enos.itcollege.ee/~ylari/I231/Quaternion.java
		// seda poleks ma kunagi välja mõelnud.
		if (!(qo instanceof Quaternion)) {
			return false;
		}
		// Siin aga on mõtet üldse arvutada
		double r = 0. , i = 0. , j = 0. , k = 0. ;
		boolean tr0 = false, tr1 = false, tr2 = false, tr3 = false;
		
		r = ( (Quaternion) qo).re;
		i = ( (Quaternion) qo).ii;
		j = ( (Quaternion) qo).jj;
		k = ( (Quaternion) qo).kk;
		
		if (Math.abs(this.re - r) < epsilon) { tr0 = true; } 
		if (Math.abs(this.ii - i) < epsilon) { tr1 = true; }
		if (Math.abs(this.jj - j) < epsilon) { tr2 = true; }
		if (Math.abs(this.kk - k) < epsilon) { tr3 = true; }
		
		if (tr0 && tr1 && tr2 && tr3) {
			return true;
		}
		return false;
	}

	/**
	 * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
	 * 
	 * @param q
	 *            factor
	 * @return dot product of this and q
	 */
	public Quaternion dotMult(Quaternion q) {
		Quaternion konn, liidetavA, liidetavB, summa, jagatis ;
		double kaks = 2. ;
		konn = q.conjugate();
		liidetavA = times(konn);
		konn = this.conjugate();
		liidetavB = q.times(konn);
		summa = liidetavA.plus(liidetavB);
		jagatis = new Quaternion (summa.re / kaks, summa.ii / kaks, summa.jj / kaks, summa.kk / kaks);
		return jagatis;
	}

	/**
	 * Integer hashCode has to be the same for equal objects.
	 * 
	 * @return hashcode
	 */
	@Override
	public int hashCode() {
		// inspiratsioon: http://stackoverflow.com/questions/9650798/hash-a-double-in-java		
		int hushHush = new Double(re + ii + jj + kk).hashCode();
		return hushHush;
	}

	/**
	 * Norm of the quaternion. Expressed by the formula norm(a+bi+cj+dk) =
	 * Math.sqrt(a*a+b*b+c*c+d*d)
	 * 
	 * @return norm of <code>this</code> (norm is a real number)
	 */
	public double norm() {
		double tagastus = 0.;
		tagastus = Math.sqrt((this.re *this.re) + (this.ii * this.ii)
				+ (this.jj * this.jj) + (this.kk * this.kk));		
		return tagastus;

	}

	/**
	 * Main method for testing purposes.
	 * 
	 * @param arg
	 *            command line parameters
	 */
	public static void main(String[] arg) {
		Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
		if (arg.length > 0)
			arv1 = valueOf(arg[0]);
		System.out.println("first: " + arv1.toString());
		System.out.println("real: " + arv1.getRpart());
		System.out.println("imagi: " + arv1.getIpart());
		System.out.println("imagj: " + arv1.getJpart());
		System.out.println("imagk: " + arv1.getKpart());
		System.out.println("isZero: " + arv1.isZero());
		System.out.println("conjugate: " + arv1.conjugate());
		System.out.println("opposite: " + arv1.opposite());
		System.out.println("hashCode: " + arv1.hashCode());
		Quaternion res = null;
		try {
			res = (Quaternion) arv1.clone();
		} catch (CloneNotSupportedException e) {
		}
		;
		System.out.println("clone equals to original: " + res.equals(arv1));
		System.out.println("clone is not the same object: " + (res != arv1));
		System.out.println("hashCode: " + res.hashCode());
		res = valueOf(arv1.toString());
		System.out.println("string conversion equals to original: "
				+ res.equals(arv1));
		Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
		if (arg.length > 1)
			arv2 = valueOf(arg[1]);
		System.out.println("second: " + arv2.toString());
		System.out.println("hashCode: " + arv2.hashCode());
		System.out.println("equals: " + arv1.equals(arv2));
		res = arv1.plus(arv2);
		System.out.println("plus: " + res);
		System.out.println("times: " + arv1.times(arv2));
		System.out.println("minus: " + arv1.minus(arv2));
		double mm = arv1.norm();
		System.out.println("norm: " + mm);
		System.out.println("inverse: " + arv1.inverse());
		System.out.println("divideByRight: " + arv1.divideByRight(arv2));
		System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
		System.out.println("dotMult: " + arv1.dotMult(arv2));
	}
}
// end of file
